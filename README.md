# dwm - dynamic window manager

[dwm][suckless-dwm] is an extremely fast, small, and dynamic window manager
for X.

## Branches

- `source`:
   - Files are from suckless except `.gitignore` and `README.md`.
   - The _source_ branch should **not be patched or customized**.
- `patch/RELEASE/feature`:
   - A _patch_ branch should be created from the _source_ branch.
   - Each feature should be patched on an **individual branch**.
   - The _RELEASE_ should correspond to the dwm version.
- `platform/RELEASE/os`:
   - A _platform_ branch should be created from the _source_ branch.
   - The _platform_ branch should **not be patched**.
   - The _RELEASE_ should correspond to the dwm version.
- `build/RELEASE/os`:
   - A _build_ branch should be created from the _source_ branch.
   - Merge the required feature or platform branches into a _build_ branch.
   - Customization should be **after merging branches**.
   - The _RELEASE_ should correspond to the dwm version.

## Requirements

In order to build dwm you need the Xlib header files.

## Installation

Edit `config.mk` to match your local setup (dwm is installed into
the `/usr/local` namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

```shell
$ make clean install
```

## Running dwm

Add the following line to your `.xinitrc` to start dwm using `startx`:

```
exec dwm
```

In order to connect dwm to a specific display, make sure that
the `DISPLAY` environment variable is set correctly, e.g.:

```
DISPLAY=foo.bar:1 exec dwm
```

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your `.xinitrc`:

```
while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
do
	sleep 1
done &
exec dwm
```

## Configuration

The configuration of dwm is done by creating a custom `config.h`
and (re)compiling the source code.

## Keyboard shortcuts

The `MODKEY` is set to `Alt`.

### General

| Function                                               | Key binding              |
|:-------------------------------------------------------|:-------------------------|
| Focus next window                                      | _MODKEY_ + j             |
| Focus previous window                                  | _MODKEY_ + k             |
| Increase master area size                              | _MODKEY_ + l             |
| Decrease master area size                              | _MODKEY_ + h             |
| Increase number of windows in master area              | _MODKEY_ + i             |
| Decrease number of windows in master area              | _MODKEY_ + d             |
| Close focused window                                   | _MODKEY_ + Shift + c     |
| Quit dwm                                               | _MODKEY_ + Shift + q     |
| Zoom/cycle focused window to/from master area          | _MODKEY_ + Enter         |
| Toggle focused window between tiled and floating state | _MODKEY_ + Shift + Space |
| Toggle bar on and off                                  | _MODKEY_ + b             |

### Tag

| Function                                             | Key binding                   |
|:-----------------------------------------------------|:------------------------------|
| View all windows with 1~9 tag                        | _MODKEY_ + 1~9                |
| Apply 1~9 tag to focused window                      | _MODKEY_ + Shift + 1~9        |
| Add/remove all windows with 1~9 tag to/from the view | _MODKEY_ + Ctrl + 1~9         |
| Add/remove 1~9 tag to/from focused window            | _MODKEY_ + Ctrl + Shift + 1~9 |
| Toggle to the previously selected tags               | _MODKEY_ + Tab                |
| View all windows with any tag                        | _MODKEY_ + 0                  |
| Apply all tags to focused window                     | _MODKEY_ + Shift + 0          |

### Layout

| Function                                   | Key binding      |
|:-------------------------------------------|:-----------------|
| Set tiled layout                           | _MODKEY_ + t     |
| Set floating layout                        | _MODKEY_ + f     |
| Set monocle layout                         | _MODKEY_ + m     |
| Toggle between current and previous layout | _MODKEY_ + Space |

### Application

| Function     | Key binding              |
|:-------------|:-------------------------|
| Open _dmenu_ | _MODKEY_ + p             |
| Open _st_    | _MODKEY_ + Shift + Enter |

### Multi-monitor

| Function                                       | Key binding          |
|:-----------------------------------------------|:---------------------|
| Focus previous screen, if any                  | _MODKEY_ + ,         |
| Focus next screen, if any                      | _MODKEY_ + .         |
| Send focused window to previous screen, if any | _MODKEY_ + Shift + , |
| Send focused window to next screen, if any     | _MODKEY_ + Shift + . |

## Mouse shortcuts

### Window

| Action                          | Function                                                                   |
|:--------------------------------|:---------------------------------------------------------------------------|
| _MODKEY_ + Left-click and drag  | Move focused window. Tiled windows will be toggled to the floating state   |
| _MODKEY_ + Right-click and drag | Resize focused window. Tiled windows will be toggled to the floating state |
| _MODKEY_ + Middle-click         | Toggle focused window between floating and tiled state                     |

### Labels on the bar

| Action                                | Function                                              |
|:--------------------------------------|:------------------------------------------------------|
| Left-click on a tag label             | Display all windows with that tag                     |
| Right-click on a tag label            | Add/remove all windows with that tag to/from the view |
| _MODKEY_ + Left-click on a tag label  | Apply that tag to the focused window                  |
| _MODKEY_ + Right-click on a tag label | Add/remove that tag to/from the focused window        |
| Left-click on the layout label        | Toggle between current and previous layout            |
| Right-click on the layout label       | Set monocle layout                                    |
| Middle-click on the title label       | Zoom/cycle focused window to/from master area         |
| Middle-click on the status label      | Open _st_                                             |

## Credits

- [Suckless project][suckless-website]

[suckless-dwm]: https://dwm.suckless.org/
[suckless-website]: https://suckless.org/
